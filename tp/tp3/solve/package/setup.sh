#!/bin/bash

# On suit la doc d'install de Docker
yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2
yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce

# Activation du démon docker au démarrage de la machine
systemctl enable --now docker

# Ajout de l'utilisateur 'vagrant' au groupe 'docker'
# Cela permet, pour rappel, d'utiliser les commandes docker sans avoir besoin des droits de root (avec sudo par exemple)
usermod -aG docker vagrant

# Ajout de docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose &> /dev/null
chmod +x /usr/local/bin/docker-compose

# Ajout de s3fs
yum install -y epel-release
yum install -y s3fs-fuse

# Démarrage des vbox invités
systemctl enable vboxadd
systemctl enable vboxadd-service

# Récupération d'images docker utilisés dans le TP
docker pull redis
docker pull python:3.7-slim
docker pull minio/minio:RELEASE.2020-03-14T02-21-58Z
docker pull traefik:v2.1.3

# Récupération de restic
yum install -y yum-plugin-copr
yum copr enable copart/restic -y
yum install -y restic

# Clean caches and shit
rm -f /etc/udev/rules.d/70-persistent-net.rules # Normalement useless, mais au cas où
yum clean all
rm -rf /tmp/*
rm -f /var/log/wtmp /var/log/btmp
history -c
