# Setup vagrant

This Vagrantfile just setup a simple machine, used to be packaged as a base box for the TP. 

It is designed to use with Vagrant+VirtualBox (it could probably run with other hypervisors with minor tweaks).

From the `package/` directory (this directory) run :
```
$ vagrant up
```

The `vagrant up` runs the `setup.sh` script inside the VM. It installs a bunch of shit.  

*It took 5min22sec on last build on my machine.*

> **Only tested on VirtualBox 6.1 with vagrant up-to-date as of 18/03/2020.**

Then, package the VM into a new Vagrant box : 
```
$ vagrant package --output centos7-docker.box
$ vagrant box add centos7-docker centos7-docker.box
```

To free space, you can also destroy this temporary VM :
```
$ vagrant destroy -f
```
