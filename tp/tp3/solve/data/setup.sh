#!/bin/bash

# PRE-FLIGHT

# Prerequisites
[[ $(id -u) -ne 0 ]] && echo 'Must be run as root. Exiting.' && exit 1
## Binaries presence
binaries=( docker curl )
declare -A bin_path
for binary in "${binaries[@]}"
do
  if [[ ! $(command -v "${binary}") ]]
  then
    echo "Command ${binary} is needed but is not available."
    exit 1
  else
    bin_path["${binary}"]=$(command -v "${binary}")
  fi
done

# Vars
## Main
declare -r MAIN_INTERFACE='eth1'
MAIN_IP=$(ip a show dev ${MAIN_INTERFACE} | grep -Po 'inet \K.*' | cut -d' ' -f1 | cut -d'/' -f1)
declare -r MAIN_IP

## Swarm specific
declare -r SWARM_DIR=/vagrant/swarm
declare -r SWARM_MGR_TOKEN="${SWARM_DIR}/mgr_token"
declare -r SWARM_MGR_IP="${SWARM_DIR}/mgr_ip"
declare -r SWARM_MGR_PORT='2377'

## Mountpoints
declare -r MINIO1_DIR='/minio1'
declare -r MINIO2_DIR='/minio2'
declare -r S3_DIR='/s3'

# CODE

# Base env
echo 'Stop SELinux and firewalld.'
setenforce 0
systemctl stop firewalld
echo 'Copy /etc/hosts file.'
cp /vagrant/hosts/hosts /etc/hosts # only works for 3 nodes

# Docker env
echo 'Enable docker.'
systemctl enable docker
echo 'Add vagrant user to docker group.'
usermod -aG docker vagrant
echo 'Setup /etc/docker/daemon.json.'
echo '{
  "insecure-registries": [ "registry.tp3:5000" ]
}' > /etc/docker/daemon.json
echo 'Restart docker.'
systemctl restart docker

# Swarm init
if [[ ! -f "${SWARM_MGR_TOKEN}" ]] ; then
  echo 'Create Docker swarm.'
  "${bin_path['docker']}" swarm init --advertise-addr "${MAIN_IP}" &> /dev/null
  mkdir -p "${SWARM_DIR}" 
  ${bin_path["docker"]} swarm join-token manager -q > "${SWARM_MGR_TOKEN}"
  echo "${MAIN_IP}" > "${SWARM_MGR_IP}"
else
  echo 'Join Docker swarm'
  "${bin_path['docker']}" swarm join --token "$(cat ${SWARM_MGR_TOKEN})" "$(cat ${SWARM_MGR_IP}):${SWARM_MGR_PORT}"
fi

# Mountpoints
echo "Creating mountpoints."
mountpoints=( "${MINIO1_DIR}" "${MINIO2_DIR}" "${S3_DIR}" )
for mountpoint in "${mountpoints[@]}"
do
  if [[ ! -d "${mountpoint}" ]]
  then
    mkdir -p "${mountpoint}"
  fi
done

# Backup service
echo 'Setup backup service.'
cp /vagrant/backup/backup.service /etc/systemd/system/backup.service
chown root:root /etc/systemd/system/backup.service
chmod 644 /etc/systemd/system/backup.service

echo 'Setup backup timer.'
cp /vagrant/backup/backup.timer /etc/systemd/system/backup.timer
chown root:root /etc/systemd/system/backup.timer
chmod 644 /etc/systemd/system/backup.timer

echo 'Run systemd timer for backup (backup /vagrant every 2 minutes in Minio bucket).'
systemctl daemon-reload &> /dev/null
systemctl enable --now backup.timer &> /dev/null
