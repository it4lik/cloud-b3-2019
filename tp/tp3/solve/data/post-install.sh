#!/bin/bash

# VARS
## Hosts
declare -r HOST1='node1.tp3'
declare -r HOST2='node2.tp3'
declare -r HOST3='node3.tp3'

## General
declare -r DATA_DIR='/vagrant'

## Pyweb
PYWEB_IMAGE='registry.tp3:5000/pyweb/pyweb:latest'

## Minio
declare -r MINIO_ACCESS_KEY='testtest'
declare -r MINIO_SECRET_KEY='testtest'

# Prerequisites
[[ $(id -u) -ne 0 ]] && echo 'Must be run as root. Exiting.' && exit 1

## Binaries presence
binaries=( docker restic )
declare -A bin_path
for binary in "${binaries[@]}"
do
  if [[ ! $(command -v "${binary}") ]]
  then
    echo "Command ${binary} is needed but is not available."
    exit 1
  else
    bin_path["${binary}"]=$(command -v "${binary}")
  fi
done

# FUNCS
registry() {
  echo 'Deploy registry on node3.'
  /usr/local/bin/docker-compose -f /vagrant/registry/docker-compose.yml up -d &> /dev/null
}

traefik() {
  # Traefik conf
  echo 'Create traefik docker network.'
  "${bin_path['docker']}" network create traefik --driver overlay --attachable  &> /dev/null

  echo 'Deploy traefik stack.'
  "${bin_path['docker']}" stack deploy -c "${DATA_DIR}/traefik/docker-compose.yml" traefik &> /dev/null
}

pyweb() {
  echo 'Build pyweb image.'
  "${bin_path['docker']}" build -t "${PYWEB_IMAGE}" "${DATA_DIR}/pyweb" &> /dev/null
  echo "Push pyweb image to registry under name : ${PYWEB_IMAGE}."
  "${bin_path['docker']}" push "${PYWEB_IMAGE}" &> /dev/null
  echo 'Deploy pyweb.'
  "${bin_path['docker']}" stack deploy -c "${DATA_DIR}/pyweb/docker-compose-stack.yml" pyweb &> /dev/null
}

minio() {
  ## Minio conf (for 3 nodes only) see https://docs.min.io/docs/deploy-minio-on-docker-swarm
  ### Secrets
  echo 'Create minio secrets.' 
  echo "${MINIO_ACCESS_KEY}" | "${bin_path['docker']}" secret create access_key - &> /dev/null
  echo "${MINIO_SECRET_KEY}" | "${bin_path['docker']}" secret create secret_key - &> /dev/null

  ### Node labels (affinity)
  echo 'Setup node affinity.' 
  "${bin_path['docker']}" node update --label-add minio1=true "${HOST1}" &> /dev/null
  "${bin_path['docker']}" node update --label-add minio2=true "${HOST2}" &> /dev/null
  "${bin_path['docker']}" node update --label-add minio3=true "${HOST3}" &> /dev/null
  "${bin_path['docker']}" node update --label-add minio4=true "${HOST3}" &> /dev/null

  echo 'Deploy minio cluster.'
  "${bin_path['docker']}" stack deploy -c "${DATA_DIR}/minio/docker-compose.yml" minio &> /dev/null

  echo 'Waiting for minio cluster to be online...'
  until curl http://minio1.tp3/minio/health/live &> /dev/null ; do
    sleep 1
  done
}

swarmprom() {
  echo 'Deploy monitoring stack.'
  "${bin_path['docker']}" stack deploy -c "${DATA_DIR}/swarmprom/docker-compose-tp3.yml" mon &> /dev/null
}

restic() {
  echo 'Create restic repo (backup).'
  RESTIC_PASSWORD='testtest' \
  AWS_ACCESS_KEY_ID='testtest' \
  AWS_SECRET_ACCESS_KEY='testtest' \
  "${bin_path['restic']}" -r s3:http://minio1.tp3/backup init
}

# CODE

registry
traefik
pyweb
minio
swarmprom
restic
