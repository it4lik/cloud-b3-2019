# TP3 solving

## Base box packaging

**FIRST, YOU NEED TO PACKAGE THE BASE BOX.** 

Go to the [dedicated README](./package/).

## Prerequisites

The lab is using shared folders between the machines, mainly to share secrets (docker swarm token for example). This feature must be supported in your Vagrant+Vitualbox installation.

## Run the lab

**From the main directory**, run :
```
$ vagrant up
```

This should :
* deploy Docker swarm
* setup services
  * [registry](https://docs.docker.com/registry/)
  * [traefik](https://docs.traefik.io/)
  * [python webapp from TP1](https://gitlab.com/it4lik/cloud-b3-2019/tree/master/tp/tp1#conteneuriser-une-application-donnée)
  * [minio](https://min.io/)
  * monitoring stack ([swarmprom](https://github.com/stefanprodan/swarmprom))
    * Grafana
    * Prometheus
    * cAdvisor
    * etc.

## Access to applications

Add this to your `hosts` file :
```
10.1.1.11 traefik.tp3 pyweb.tp3 minio1.tp3 minio2.tp3 minio3.tp3 minio4.tp3 grafana.tp3 prometheus.tp3
```

Once `vagrant up` has finish running, you can visit in a web browser :
* [Traefik dashboard](http://traefik.tp3)
* [Grafana](http://grafana.tp3) (admin/admin)
* [Python web app](http://pyweb.tp3)
* [Prometheus](http://prometheus.tp3)
* Minio WebUI :
  * [minio1](http://minio1)
  * [minio2](http://minio2)
  * [minio3](http://minio3)
  * [minio4](http://minio4)

## Check backups

Backups are setup to run every 2 minutes (test only). Backup use :
* [restic](https://github.com/restic/restic) to effectively issue backups
* [minio](https://min.io/) as a storage backend for backups

---

If your setup is not fast enough, creation of the restic repo may have failed during deployment (last service deployed, when `vagrant up`ing the last node). It's not a problem.

You can create it manually with :
```
$ vagrant ssh node3.tp3

# C'est une seule commande les 4 lignes qui suivent hein. Faut tout copy/paste.
$ RESTIC_PASSWORD='testtest' \
  AWS_ACCESS_KEY_ID='testtest' \
  AWS_SECRET_ACCESS_KEY='testtest' \
  restic -r s3:http://minio1.tp3/backup init 
```

---

Backup is setup through a systemd service :
```
$ systemctl status backup
$ systemctl status backup.timer

# To see last and next executon of backup
$ systemctl list-timers
```

> Some backups may have failed while the machines are starting up. Minio is one of the last service deployed at the end of `vagrant up`.

You can also see the `backup` bucket in Minio browser on either node ([minio1](http://minio1), [minio2](http://minio2), [minio3](http://minio3), [minio4](http://minio4)).
