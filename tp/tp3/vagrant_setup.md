# Setup Vagrant environment

- [Setup Vagrant environment](#setup-vagrant-environment)
  - [Obvious (or is it ?)](#obvious-or-is-it)
  - [Création d'un box de base](#cr%c3%a9ation-dun-box-de-base)
  - [Vagrantfile TP](#vagrantfile-tp)

## Obvious (or is it ?)

* install Vagrant
* install d'un hyperviseur (VirtualBox ?)
* éventuelle config intermédiaire
* test du bon fonctionnement

```bash
$ mkdir test
$ cd test
$ vagrant init centos/7

# Cela génère un fichier Vagrantfile dans le dossier courant
# Vous pouvez le pimp un peu pour qu'il soit encore plus léger
# Par exemple

$ cat Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"

  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = true
  
    # Customize the amount of memory on the VM:
    vb.memory = "1024"
  end
end

# Test
$ vagrant up
[...]
$ vagrant ssh
```

## Création d'un box de base

On va setup une box qui contient CentOS7, avec docker pré-installé et configuré. Le but étant d'accélérer les tests pendant le TP, et mettre un peu les mains dans le provisionnement automatisée de machines virtuelles.

```ruby
$ mkdir package
$ cd package
$ vagrant init centos/7

# Configurez le Vagrantfile de façon minimaliste
$ cat Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"

  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = true

    # Customize the amount of memory on the VM:
    vb.memory = "1024"
  end
end

# On peut allumer la VM
$ vagrant up
```

L'idée est d'utiliser cette VM lancée avec Vagrant, installer et configurer des trucs dedans, puis en resortir et la définir comme une nouvelle box de base.  
On pourra alors instancier de nouvelles VM à partir de cette box créée de nos mains :). 

```ruby
# Let's gooooo
$ vagrant ssh

# CE QUI SUIT SE PASSE DANS LA VM

# On suit la doc d'install de Docker
$ sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2
$ sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
$ sudo yum install -y docker-ce

# Activation du démon docker au démarrage de la machine
$ sudo systemctl enable docker

# Ajout de l'utilisateur 'vagrant' au groupe 'docker'
# Cela permet, pour rappel, d'utiliser les commandes docker sans avoir besoin des droits de root (avec sudo par exemple)
$ sudo usermod -aG docker vagrant

# Et c'est touuut
# On clean vite fait not' merdier
$ rm -f /etc/udev/rules.d/70-persistent-net.rules # Normalement useless, mais au cas où
$ yum clean all
$ rm -rf /tmp/*
$ rm -f /var/log/wtmp /var/log/btmp
$ history -c

$ exit
```

Okay on a notre VM qui est prête ! Now, on la transforme en box Vagrant.

```bash
# Sur l'hôte, toujours dans le dossier du Vagrantfile. On a pas bougé quoi.
$ vagrant package --output centos7-docker.box
$ vagrant box add centos7-docker centos7-docker.box

# Pour vérifier
$ vagrant box list
```

Maintenant on peut se servir de la box nouvellement créée pour créer des VMs.

## Vagrantfile TP

Récap du besoin :
* 3 machines
* une interface qui permet de communiquer sur un réseau LAN
* stockage
  * 1 disque sup pour node1
  * 1 disque sup pour node2
  * 2 disques sup pour node3
* on va désactiver deux comportements par défaut de vagrant sur les box créées à la mano :
  * désactivation de la synchro du dossier `./` vers `/vagrant`
  * désactivation de l'installation automatique des installations invité
    * c'est ça qui prenait 10 fuckin plombes
    * il dl les librairies du noyau, `gcc` pour compiler, télécharge les sources des bails invités, compile, puis enfiiiin se lance

**NOTE** : il sera peut-être nécessaire de modifier des choses dans le Vagrantfile : 
* le nom du contrôleur de stockage pour vos disques
  * chez moi c'est `IDE`, ptet autre chose chez vous
  * pour tester, lancer une VM simple avec Vagrant et faites :
```
$ VBoxManage list vms
# repérez votre VM

$ VBoxManage showvminfo <NOM_VM>
$ VBoxManage showvminfo <NOM_VM> | grep UUID   # ça sort direct
# Y'a des lignes avec le nom du disque principal, et quel contrôleur il utilise :)

# Pour les Windows-users, VBoxManage.exe se trouve dans le même dossier que VirtualBox.exe
```

Le Vagrantfile, ça donne ça :
```ruby
Vagrant.configure("2") do |config|
  # Base box
  config.vm.box = "centos7-docker"

  # Disable auto install of vbguest
  config.vbguest.auto_update = false
  # Disable default synced folder
  config.vm.synced_folder '.', '/vagrant', disabled: true

  # Default config for all VMs
  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = true
    # Customize the amount of memory on the VM
    vb.memory = "1024"
  end

  # Setup 3 machines
  (1..3).each do |i|
    config.vm.define "node#{i}.tp3" do |node|
      # Network config
      node.vm.hostname = "node#{i}.tp3"
      node.vm.network :private_network, ip: "10.1.1.#{10+i}"

      # Storage config
      node.vm.provider "virtualbox" do |vb|
        ## Create a 5Go disk for each VM
        disk_name = "node#{i}-disk2.vdi"
        unless File.exist?(disk_name)
          vb.customize ['createhd', '--filename', disk_name, '--variant', 'Fixed', '--size', 5 * 1024]
        end
        vb.customize [ "storageattach", :id , "--storagectl", "IDE", "--port", "1", "--device", "0", "--type", "hdd", "--medium", disk_name]

        ## Create an additional 5G disk for node3.tp3
        if(i == 3)
          disk_name = "node#{i}-disk3.vdi"
          unless File.exist?(disk_name)
            vb.customize ['createhd', '--filename', disk_name, '--variant', 'Fixed', '--size', 5 * 1024]
          end
          vb.customize [ "storageattach", :id , "--storagectl", "IDE", "--port", "1", "--device", "1", "--type", "hdd", "--medium", disk_name]
        end
      end
    end
  end
end
```
