| Etudiant   | Process | API | unshare | ct ns | nsenter | user ns | network | cg docker | capa | capa ping | nginx capa | Format | Note |
| ---------- | ------- | --- | ------- | ----- | ------- | ------- | ------- | --------- | ---- | --------- | ---------- | ------ | ---- |
| benelfahsi | -       | x   | x       | x     | x       | x       | x       | x         | x    | x         | -          | x      | 16   |
| blanchet   | x       | -   | -       | -     | x       | -       | x       | x         | x    | x         | -          | x      | 14   |
| clemencon  | x       | x   | x       | x     | x       | o       | x       | x         | x    | x         | +          | x      | 16   |
| dinh       | +       | x   | x       | x     | x       | x       | x       | x         | x    | x         | +          | x      | 18   |
| ivan       | +       | o   | x       | x     | o       | o       | -       | -         | -    | +         | o          | x      | 14   |
| jeleff     | x       | x   | x       | x     | x       | -       | -       | +         | -    | o         | o          | o      | 13   |
| lecossec   | o       | o   | -       | -     | x       | o       | x       | -         | -    | +         | x          | x      | 14   |
| rougagnou  | x       | x   | x       | x     | x       | x       | x       | +         | x    | x         | +          | x      | 19   |
| wilfart    | x       | x   | x       | x     | x       | x       | x       | x         | o    | o         | o          | x      | 15   |
| bacle      | x       | x   | x       | -     | x       | -       | x       | x         | x    | x         | -          | x      | 16   |
| estienne   | x       | x   | x       | x     | x       | -       | x       | x         | x    | x         | -          | x      | 16   |
| segonne    | x       | o   | x       | x     | x       | o       | -       | x         | x    | -         | -          | x      | 14   |