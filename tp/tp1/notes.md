| Etudiant   | NGINX | Image Python | Volume Docker | Conf démon | dcv1 | dcv2 | Dockerfile | Dépôt | Format | Note |
| ---------- | ----- | ------------ | ------------- | ---------- | ---- | ---- | ---------- | ----- | ------ | ---- |
| bacle      | x     | x            | x             | x          | x    | x    | x          | x     | x      | 17   |
| benelfahsi | x     | x            | x             | x          | x    | o    | o          | o     | o      | 12   |
| blanchet   | x     | x            | x             | x          | x    | x    | x          | o     | o      | 12   |
| clemencon  | x     | x            | o             | o          | o    | o    | o          | o     | x      | 9    |
| dinh       | x     | x            | x             | x          | x    | x    | x          | x     | x      | 17   |
| estienne   | x     | x            | x             | x          | x    | x    | x          | o     | o      | 14   |
| germain    | x     | x            | x             | x          | x    | x    | x          | o     | x      | 14   |
| ivan       | x     | x            | x             | o          | o    | o    | o          | o     | o      | 9    |
| jeleff     | x     | x            | x             | x          | x    | o    | o          | o     | -      | 11   |
| lecossec   | o     | x            | o             | -          | x    | x    | x          | o     | o      | 13   |
| ollivier   | x     | x            | x             | x          | x    | x    | x          | o     | x      | 14   |
| ricaud     | x     | x            | x             | x          | x    | o    | o          | o     | x      | 12   |
| wilfart    | x     | x            | x             | x          | x    | x    | x          | o     | o      | 12   |
| rougagnou  | x     | x            | x             | x          | x    | x    | x          | x     | x      | 18   |
| segonne    | x     | x            | o             | o          | x    | -    | -          | o     | -      | 12   |